#include <gtk/gtk.h>
// #include "functions.h"
#include "foo.h"

GtkWidget *g_lbl_hello;
GtkWidget *g_lbl_count;
GtkWidget *g_lbl_pirate;

typedef struct {
    GtkWidget *w_lbl_nb_update;
    GtkWidget *w_lbl_time;
} app_widgets;


int main(int argc, char *argv[])
{
    GtkBuilder      *builder; 
    GtkWidget       *window;
    app_widgets     *widgets = g_slice_new(app_widgets);

    gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "glade/window_main.glade", NULL);

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    
    
    // get pointers to the two labels
    g_lbl_hello = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_hello"));
    g_lbl_count = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_count"));
    g_lbl_pirate = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_pirate"));

    widgets->w_lbl_time  = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_time"));
    widgets->w_lbl_nb_update = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_nb_update"));

    gtk_builder_connect_signals(builder, widgets);
    g_object_unref(builder);

    gtk_widget_show(window);                
    gtk_main();
    g_slice_free(app_widgets, widgets);

    return 0;
}


// called when button is clicked
void on_btn_hello_clicked()
{
    static unsigned int count = 0;
    char str_count[30] = {0};

    gtk_label_set_text(GTK_LABEL(g_lbl_hello), "Hello, world!");
    count++;
    sprintf(str_count, "%d", count);
    gtk_label_set_text(GTK_LABEL(g_lbl_count), str_count);
}


void on_btn_balloon_clicked(GtkButton *button, GtkLabel *text_label)
{
    gtk_label_set_text(text_label, "You clicked the button!");
    //myadd(2, 4);
    foo(3);
}

void on_button1_clicked()
{
    
    gtk_label_set_text(GTK_LABEL(g_lbl_pirate), "AAR!");
  
}


void on_btn_time_update_clicked(GtkButton *button, app_widgets *app_wdgts)
{
    GDateTime   *time;            // for storing current time and date
    gchar       *time_str;        // current time and date as a string
    static gint count = 0;        // stores number of times button was clicked
    gchar       *count_str;        // button clicked count as a string
    
    time     = g_date_time_new_now_local();             // get the current time
    time_str = g_date_time_format(time, "%H:%M:%S");    // convert current time to string
    count++;                                            // number of times button clicked
    count_str = g_strdup_printf("%d", count);           // convert count to string
    
    // update time and count in label widgets
    gtk_label_set_text(GTK_LABEL(app_wdgts->w_lbl_time), time_str);
    gtk_label_set_text(GTK_LABEL(app_wdgts->w_lbl_nb_update), count_str);
    
    // free memory used by glib functions
    g_free(time_str);
    g_date_time_unref(time);
    g_free(count_str);
}


// handle radio button toggle events
void on_rb_1_toggled (GtkToggleButton *togglebutton, GtkLabel *label)
{
    gtk_label_set_text(label, "1%");     // update label
    g_print("Button 1 Toggled\n");       // print to command line
}

void on_rb_2_toggled (GtkToggleButton *togglebutton, GtkLabel *label)
{
    gtk_label_set_text(label, "2%");
    g_print("Button 2 Toggled\n");
}

void on_rb_3_toggled (GtkToggleButton *togglebutton, GtkLabel *label)
{
    gtk_label_set_text(label, "3%");
    g_print("Button 3 Toggled\n");
}


// called when window is closed
void on_window_main_destroy()
{
    gtk_main_quit();
}