#include "foo.h"  /* Include the header (not strictly necessary here) */
#include <stdio.h>

int foo(int x)    /* Function definition */
{
    printf("Added value=%d\n", x + 5);
    return x + 5;
}