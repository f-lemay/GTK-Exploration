# change application name here (executable output name)
TARGET=main.out
# dependancies
DEPS=foo.o
# compiler
CC=gcc
# debug
DEBUG=-g
# optimisation
OPT=-O0
# warnings
WARN=-Wall
# source directory
SRCDIR=src/

PTHREAD=-pthread

CCFLAGS= $(DEBUG) $(OPT) $(WARN) $(PTHREAD) -pipe

GTKLIB=`pkg-config --cflags --libs gtk+-3.0`

# linker
LD=gcc
LDFLAGS=$(PTHREAD) $(GTKLIB) -export-dynamic

OBJS=	main.o $(DEPS)

.PHONY : all clean

all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)
    
main.o:	$(SRCDIR)main.c
	$(CC) -c $(CCFLAGS) $(SRCDIR)main.c $(GTKLIB) -o main.o

functions.o:	$(SRCDIR)functions.c
	$(CC) -c $(CCFLAGS) $(SRCDIR)functions.c $(GTKLIB) -o functions.o

# -o foo.o is optional
foo.o:	$(SRCDIR)foo.c
	$(CC) -c $(SRCDIR)foo.c $(GTKLIB)
    
clean:
	rm -f *.o $(TARGET)
